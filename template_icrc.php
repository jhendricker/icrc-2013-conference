<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

wp_enqueue_style('home',get_template_directory_uri() . '/home.css',array('site'));

get_header(); ?>

	<div id="torso">
	
		<div class="container">

			<?php
				while (have_posts()) { the_post();
					?>

					<a href="http://www.rsvpbook.com/ICRC2013" target="_blank">
						<?php the_post_thumbnail('slide') ?>
					</a>
				
					<div id="content">

						<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
							<div class="title"><span>icrc conference:</span></div>
							<div class="entry">
								<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
								<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
							</div>
						</div>
					
					</div>

					<?php
				} ?>

			<div id="mods">

				<?php
				$args = array(
					'post_type' => 'homemod',
					'posts_per_page' => 3,
					'orderby' => 'menu_order',
					'order' => 'asc',
					'tax_query' => array(
						array(
							'taxonomy' => 'homepage',
							'field' => 'slug',
							'terms' => 'icrc'
						)
					)
				);
				$mods = new WP_Query($args);
				if ($mods->have_posts())
					while ($mods->have_posts()) {
						$mods->the_post();

						$btn = get_post_meta(get_the_ID(),'_btn',true);
						$button = '';
						if (is_array($btn) && count($btn) && !empty($btn['text']))
							$button = '<a class="btn" href="' . $btn['link'] . '">' . stripslashes($btn['text']) . '</a>';

						$title = '';
						if (has_post_thumbnail()) {
							$title = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
							$title = ' style="height: ' . $title[2] . 'px; background-image: url(' . $title[0] . ');"';
						}
						?>

						<div id="<?php echo $post->post_name ?>" class="mod">
							<h3 class="title<?php echo has_post_thumbnail() ? ' image' : ''; ?>"<?php echo $title ?>><?php the_title() ?></h3>
							<div class="entry">
								<?php the_content() ?>
							</div>
							<?php echo $button ?>
						</div>

						<?php
					}
				?>
			
			</div>

			<div id="sponsors">

				<div class="title"><span>Sponsors:</span></div>

				<ul>

					<?php
					$sponsors = new WP_Query('post_type=sponsor&posts_per_page=6&orderby=rand');
					if ($sponsors->have_posts())
						while ($sponsors->have_posts()) {
							$sponsors->the_post();

							echo '<li id="post-' . get_the_ID() . '">';
								MultiPostThumbnails::the_post_thumbnail('sponsor','grayscale-image',$post->ID,'thumbnail');
							echo '</li>';

						}
					?>

				</ul>
			
			</div>

		</div>

	</div>

<?php get_footer(); ?>
