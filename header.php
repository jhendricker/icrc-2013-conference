<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
	
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	
	<title><?php wp_title(''); ?></title>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_enqueue_style('fonts','//fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Pinyon+Script|Quattrocento') ?>
	<?php wp_enqueue_style('site',get_bloginfo('stylesheet_url'),array('fonts')) ?>
		
	<?php wp_enqueue_script('jquery') ?>
	<?php wp_enqueue_script('jquery-color') ?>
	<?php wp_enqueue_script('validate',get_template_directory_uri() . '/js/validate.js',array('jquery'),'1.0') ?>
	<?php wp_enqueue_script('site',get_template_directory_uri() . '/js/scripts.js',array('jquery','jquery-color','validate'),'1.0') ?>
	
	<?php wp_head(); ?>
	
</head>
<body <?php body_class(); ?>>

	<div id="header">

		<div class="container">

			<a id="logo" href="<?php echo esc_url( home_url() ) ?>">Center for Crisis Communication</a>

			<ul id="social">
				<li class="fb"><a href="<?php echo FACEBOOK ?>">Like us on Facebook</a></li>
				<li class="tw"><a href="<?php echo TWITTER ?>">Follow us on Twitter</a></li>
				<li class="in"><a href="<?php echo LINKEDIN ?>">View us on LinkedIn</a></li>
				<li class="yt"><a href="<?php echo YOUTUBE ?>">Watch us on YouTube</a></li>
			</ul>

			<a id="email" href="<?php echo antispam('mailto:' . EMAIL) ?>"><?php echo antispam(EMAIL) ?></a>

		</div>

	</div>

	<div id="nav">
		<div class="container">
			<?php wp_nav_menu(array('theme_location' => 'primary','depth' => 2)); ?>
		</div>
	</div>
