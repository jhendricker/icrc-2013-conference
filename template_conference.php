<?php
/**
 * @package WordPress
 * @subpackage Default_Theme 
 * Template Name: Conference 
 */

get_header(); ?>

	<div id="torso">

		<div class="container">

			<div id="content">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
						<div class="title"><h1 class="pagetitle"><?php the_title() ?></h1></div>
						<?php
						echo is_page('contact') ? '<a href="http://goo.gl/maps/J6CN8" title="Google Maps" target="_blank">' : '';
						if (has_post_thumbnail())
							the_post_thumbnail('content');
						echo is_page('contact') ? '</a>' : '';
						?>
						<div class="entry">
							<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
							<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
						</div>
					</div>

				<?php endwhile; endif; ?>

			</div>

			<div id="sidebar">
				<?php dynamic_sidebar('Conference'); ?>
			</div>

		</div>

	</div>

<?php get_footer(); ?>
