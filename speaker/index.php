<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

$custom = new setup_speaker_post_type();
$custom->index();

get_header();

global $wp_query;
$original_query = $wp_query->query;

$new_q = array();
$new_q['post_type'] = $custom->type;
$new_q['orderby'] = 'menu_order';
$new_q['order'] = 'asc';
$new_q['paged'] = get_query_var( 'paged' );
$new_q['posts_per_page'] = -1;

$customs = new WP_Query($new_q);
?>

	<div id="torso">

		<div class="container">

			<div id="content">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div <?php post_class() ?> id="post-<?php the_ID(); ?>" style="margin-bottom: 30px;">

						<div class="title"><h1 class="pagetitle"><?php the_title() ?></h1></div>

						<div class="entry">
							<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
							<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
						</div>

					</div>

				<?php endwhile; endif; ?>

				<?php if ($customs->have_posts()) : ?>

					<?php while ($customs->have_posts()) : $customs->the_post();
						$img = wp_get_attachment_image_src(get_post_thumbnail_id(),'thumbnail');
						$style = ' style="background-image: url(' . $img[0] . ');"';
						?>

						<a href="<?php the_permalink() ?>" <?php post_class() ?> id="post-<?php the_ID(); ?>"<?php echo $style ?>>

							<span>
								<?php the_title() ?>
							</span>

						</a>

					<?php endwhile; ?>

					<div class="navigation">
						<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
						<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
					</div>

				<?php else : ?>

					<h2 class="center">Not Found</h2>
					<p class="center">Sorry, but you are looking for something that isn't here.</p>
					<?php get_search_form(); ?>

				<?php endif; ?>

			</div>

			<div id="sidebar">
				<?php dynamic_sidebar('Speakers') ?>
			</div>

		</div>

	</div>

<?php get_footer(); ?>
