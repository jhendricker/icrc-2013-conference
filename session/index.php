<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

$custom = new setup_session_post_type();
$custom->index();

get_header();

global $wp_query;
$original_query = $wp_query->query;

$new_q = array();
$new_q['post_type'] = $custom->type;
$new_q['orderby'] = 'date';
$new_q['order'] = 'desc';
$new_q['paged'] = get_query_var( 'paged' );
$new_q['posts_per_page'] = -1;

$customs = new WP_Query($new_q);
?>

	<div id="torso">

		<div class="container">

			<div id="content">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div <?php post_class() ?> id="post-<?php the_ID(); ?>" style="margin-bottom: 30px;">

						<div class="title"><h1 class="pagetitle"><?php the_title() ?></h1></div>

						<div class="entry">
							<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
							<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
						</div>

					</div>

				<?php endwhile; endif; ?>

				<?php if ($customs->have_posts()) : ?>

					<?php while ($customs->have_posts()) : $customs->the_post(); ?>

						<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

							<h2 id="<?php echo $post->post_name ?>"><?php the_title(); ?></h2>

							<div class="entry">

								<?php
								$speaker = get_post_meta(get_the_ID(),'_speaker',true);
								if (isset($speaker) && !empty($speaker))
									if (has_post_thumbnail($speaker))
										echo get_the_post_thumbnail($speaker,'thumbnail');
								?>
								
								<?php the_content('Read the rest of this entry &raquo;'); ?>

							</div>

						</div>

					<?php endwhile; ?>

					<div class="navigation">
						<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
						<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
					</div>

				<?php else : ?>

					<h2 class="center">Not Found</h2>
					<p class="center">Sorry, but you are looking for something that isn't here.</p>
					<?php get_search_form(); ?>

				<?php endif; ?>

			</div>

			<div id="sidebar">
				<?php dynamic_sidebar('Conference') ?>
			</div>

		</div>

	</div>

<?php get_footer(); ?>
