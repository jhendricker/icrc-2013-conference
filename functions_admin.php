<?php

add_editor_style();

add_action('login_enqueue_scripts','login_includes');
function login_includes() {
	echo "<link rel='stylesheet' id='custom-admin-css'  href='" . get_template_directory_uri() . "/login.css' type='text/css' media='all' />";
}

add_action('admin_enqueue_scripts','admin_includes');
function admin_includes() {
	wp_enqueue_style('fonts','//fonts.googleapis.com/css?family=Quattrocento');
	wp_enqueue_style('custom-admin',get_template_directory_uri() . '/admin.css');
}

add_action('add_meta_boxes','cpmbs');
function cpmbs() {
	global $post;

	if ($post->post_parent > 0) {
		$parent = $post->post_parent;
		$type = get_post_type_object(get_post_type($parent));
		$title = '<a href="' . get_edit_post_link($parent) . '" title="Edit \'' . get_the_title($parent) . '\'">&larr; Edit ' . apply_filters('parents_this_type',$type->labels->singular_name) . '\'s Parent</a>';
		add_meta_box('edit_parent',$title,'cpmb_edit_parent','','side');
	}

}

function cpmb_edit_parent() {
	return;
}

add_action('admin_notices','warn_deleting_idx_template');
function warn_deleting_idx_template() {
	global $post;

	if( isset($post->page_template) ) {	
		if ($post->page_template != 'template_idx.php')
			return;
	}
	
	echo '<div class="error">
       <p style="line-height: 20px;"><span style="font-size: 18px;">DO NOT DELETE THIS PAGE: BAD THINGS <EM>WILL</EM> HAPPEN!</span><br />Your IDX uses this page as reference for the header and footer: deleting this page will break your search and property pages.<br />Also, editing this page will not change anything on your site; best to just leave it alone.</p>
    </div>';
}

add_filter('admin_footer_text','admin_footer_text',11);
function admin_footer_text($content) {
	$search = 'WordPress</a>';
	$len = strpos($content,$search) + strlen($search);
	return substr($content,0,$len) . ' and choosing <a href="http://www.brandco.com" target="_blank">BrandCo</a>' . substr($content,$len);
}




/*$config = new config();

add_field(array('social','facebook'));

function add_field_group($id,$name = '',$desc = '') {
    global $config;
    
    if (empty($name))
        $name = ucwords($id);

    $config->groups[$id] = array($name,$desc);
}

function add_field($id,$default = '',$label = '') {
    global $config;
    
    $group = '';
    if (is_array($id)) {
        $group = $id[0];
        $id = $id[1];
        if (!array_key_exists($group,$config->groups))
            add_field_group($group);
    }

    $config->fields[$group][] = array($id,$default,$label);
}

$config->init();

class config {

	var $cap = 'edit_others_posts';

        var $groups = array();
	var $fields = array();

	// Social media: Facebook, Twitter, LinkedIn, YouTube, FourSquare, Google+, Pinterest
	// Connect: address (city, state, zip), email, phone, fax, office, second phone
	// URLs: IDX, Google Maps directions
	// Text: copyright
	// other data (expandable)

	function init() {
		add_action('admin_menu',array($this,'admin_menu'));
	}

	function admin_menu() {
		add_menu_page('Config','Config',$this->cap,'bco_config',array(&$this,'main'),'',3);
	}

	function main() {



	}
}*/

?>
