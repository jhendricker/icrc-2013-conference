<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

$setup = new setup_custom_post_type;
$setup->init();
unset($setup);

class setup_custom_post_type {

	var $type = 'customtype';
	var $single = 'Custom';
	var $plural = 'Customs';
	var $nonce = 'customtype';

	function init() {
		add_action('init',array(&$this,'create_post_type'));

		if (is_admin())
			$this->admin_init();
		else
			$this->public_init();
	}

	function admin_init() {
		//add_action('admin_init',array(&$this,'meta_boxes'));
		//add_action('save_post',array(&$this,'save_post'));
		//add_action("manage_{$this->type}_posts_custom_column", array(&$this,"custom_post_column_values"));
		//add_filter("manage_edit-{$this->type}_columns", array(&$this,"custom_post_column_titles"));
	}

	function public_init() {
		add_action('wp_print_styles',array(&$this,'styles'));
		add_filter('nav_menu_css_class',array(&$this,'special_nav_class'),10,2);
	}
	
	function index() {
		wp_enqueue_style($this->type,get_template_directory_uri() . '/' . $this->type . '/style.css');
		add_filter('body_class',array(&$this,'custom_type_body_classes'));
	}
	
	function custom_type_body_classes($classes) {
		if (!is_tax() && !is_category() && !is_archive())
			$classes[] = $this->type . '-index';
		return $classes;
	}

	function special_nav_class($classes, $item) {
		global $wp_query;
		if ($wp_query->posts[0]->post_type == $this->type)
			if ($item->title == $this->plural)
				$classes[] = 'current-menu-item';
		return $classes;
	}

	function create_post_type() {
		register_post_type($this->type, array(
			'label' => $this->plural,
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menu' => false,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => ''),
			'query_var' => true,
			'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes',),
			'labels' => array (
				'name' => $this->plural,
				'singular_name' => $this->single,
				'menu_name' => $this->plural,
				'add_new' => 'Add '.$this->single,
				'add_new_item' => 'Add New '.$this->single,
				'edit' => 'Edit',
				'edit_item' => 'Edit '.$this->single,
				'new_item' => 'New '.$this->single,
				'view' => 'View '.$this->single,
				'view_item' => 'View '.$this->single,
				'search_items' => 'Search '.$this->plural,
				'not_found' => 'No '.$this->plural.' Found',
				'not_found_in_trash' => 'No '.$this->plural.' Found in Trash',
				'parent' => 'Parent '.$this->single,
			),
		) );
	}

	function meta_boxes() {
		$positions = array('normal','side','advanced');
		add_meta_box('id','title',array(&$this,'create_meta_box'),$this->type,$positions[0]);
	}

		function create_meta_box() {
			echo '<input type="hidden" name="' . $this->nonce . '_noncename" id="' . $this->nonce . '_noncename" value="' . wp_create_nonce( __FILE__ ) . '" />';
		}

	function save_post($post_id) {
		if (!is_admin())
			return;
		
		if ( !wp_verify_nonce( $_POST[$this->nonce . '_noncename'], __FILE__ ))
			return $post_id;
		
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
			return $post_id;
			
		if (!current_user_can('edit_post',$post_id))
			return $post_id;
	}

	function custom_post_column_titles($columns) {
		global $post;
		if ($post->post_type == $this->type)
			$columns = array(
				"cb" => "<input type=\"checkbox\" />",
				"title" => "Title",
				"custom" => "custom",
			);
		return $columns;
	}
	
	function custom_post_column_values($column) {
		global $post;
		if ($post->post_type == $this->type) {
			if ("ID" == $column) echo $post->ID;
			elseif ("custom" == $column) {}
		}
	}

	function styles() {
		global $wp;

		if ($wp->query_vars["post_type"] != $this->type)
			return;
		
		$path = get_template_directory() . '/' . $this->type . '/style.css';
		$url = get_stylesheet_directory_uri() . '/' . $this->type . '/style.css';
		if (file_exists($path))
			wp_enqueue_style($this->type.'-style',$url);
	}

}

?>