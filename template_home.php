<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

wp_enqueue_script('orbit',get_template_directory_uri() . '/js/orbit.js',array('jquery'));
wp_enqueue_style('orbit',get_template_directory_uri() . '/js/orbit.css');
wp_enqueue_style('home',get_template_directory_uri() . '/home.css',array('site','orbit'));

get_header(); ?>

	<div id="torso">
	
		<div class="container">

			<div id="slides_container">
			
				<div id="slides">

					<?php
					$slides = new WP_Query('post_type=slide&posts_per_page=-1&orderby=menu_order&order=asc');
					while ($slides->have_posts()) {
						$slides->the_post();
						$link = get_post_meta(get_the_ID(),'_link',true);

						if (!empty($link))
							echo '<a id="post-' . get_the_ID() . '" href="' . get_post_meta(get_the_ID(),'_link',true) . '">';

							the_post_thumbnail('slide');

						if (!empty($link))
							echo '</a>';
					}
					wp_reset_query();
					?>
				
				</div>

			</div>
		
			<div id="content">

				<?php
				while (have_posts()) { the_post();
					?>

					<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
						<div class="title"><span>welcome:</span></div>
						<div class="entry">
							<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
							<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
						</div>
					</div>

					<?php
				} ?>
			
			</div>

			<div id="mods">

				<?php
				$args = array(
					'post_type' => 'homemod',
					'posts_per_page' => 3,
					'orderby' => 'menu_order',
					'order' => 'asc',
					'tax_query' => array(
						array(
							'taxonomy' => 'homepage',
							'field' => 'slug',
							'terms' => 'ccc'
						)
					)
				);
				$args['tax_query'][0]['terms'] = 'icrc';
				$mods = new WP_Query($args);
				if ($mods->have_posts())
					while ($mods->have_posts()) {
						$mods->the_post();

						$btn = get_post_meta(get_the_ID(),'_btn',true);
						$button = '';
						if (is_array($btn) && count($btn) && !empty($btn['text']))
							$button = '<a class="btn" href="' . $btn['link'] . '">' . stripslashes($btn['text']) . '</a>';

						$title = '';
						if (has_post_thumbnail()) {
							$title = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
							$title = ' style="height: ' . $title[2] . 'px; background-image: url(' . $title[0] . ');"';
						}
						?>

						<div id="<?php echo $post->post_name ?>" class="mod">
							<h3 class="title<?php echo has_post_thumbnail() ? ' image' : ''; ?>"<?php echo $title ?>><?php the_title() ?></h3>
							<div class="entry">
								<?php the_content() ?>
							</div>
							<?php echo $button ?>
						</div>

						<?php
					}
				?>
			
			</div>

		</div>

	</div>

<?php get_footer(); ?>
