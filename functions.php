<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */


add_theme_support('post-thumbnails');

add_image_size('slide',940,385);
add_image_size('content',610);

if (class_exists('MultiPostThumbnails')) {
	new MultiPostThumbnails(
		array(
			'label' => 'Grayscale Image',
			'id' => 'grayscale-image',
			'post_type' => 'sponsor'
		)
	);
	new MultiPostThumbnails(
		array(
			'label' => 'Banner Image',
			'id' => 'speaker-banner',
			'post_type' => 'speaker'
		)
	);
}

if ( function_exists( 'register_nav_menu' ) ) {
	register_nav_menu('primary','Primary Navigation');
	register_nav_menu('footer','Footer Navigation');
	register_nav_menu('footer-ccc','Footer CCC Navigation');
	register_nav_menu('footer-icrc','Footer ICRC Navigation');
}

require_once('homemod/setup_homemod.php');
require_once('session/setup_session.php');
require_once('speaker/setup_speaker.php');
require_once('sponsor/setup_sponsor.php');
require_once('slide/setup_slide.php');

require_once('functions_widgets.php');

$is_login = (strpos($_SERVER['PHP_SELF'],'wp-login.php') ? true : false);

if (is_admin() || $is_login)
	require_once('functions_admin.php');
else if (!$is_login)
	require_once('functions_public.php');

?>