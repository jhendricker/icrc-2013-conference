// JavaScript Document

jQuery(function($) {

	jQuery.fn.hoverColor = function(args) {
		var config = {
			'on' : '#FFF',
			'off' : 'black',
			'time' : '150'
		}

		$.extend(config,args);

		return this.each(function() {
			$(this).hover(function() {
				if ($(this).css('cursor') != 'default')
					$(this).stop(true).animate({color: config.on},config.time);
			},function() {
				$(this).stop(true).animate({color: config.off},config.time);
			});
		});
	};

	$("#nav ul.menu > li > a").hoverColor({on: '#EFC201',off: '#FFF'});
	$("#nav ul.sub-menu > li > a").hoverColor({on: '#FFF',off: '#EFC201'});

	var timeouts = new Array();
	$("#nav ul.menu > li").hover(function() {
		clearTimeout(timeouts[$(this).attr('id')]);
		$(this).find('ul.sub-menu').stop(true).fadeTo(200,1);
	},function() {
		var drop = $(this);
		timeouts[drop.attr('id')] = setTimeout(function() {
			drop.find('ul.sub-menu').stop(true).fadeTo(200,0,function() { $(this).hide(); });
		},300);
	});

	$("#nav ul.sub-menu").hover(function() {
		clearTimeout($(this).parent().attr('id'));
	},function() {
	});
    
	$("form li input[type=text],form li input[type=email],form li textarea,#sidebar #searchform input[type=text]").each(function() {
		if ($(this).val() != "")
			$(this).parent().find("label").hide();
	}).focus(function() {
		$(this).parent().find("label").addClass("focus");
	}).keypress(function() {
		$(this).parent().find("label").hide();
	}).blur(function() {
		if ($(this).val() == "")
			$(this).parent().find("label").removeClass("focus").fadeIn();
	});

	$("#social li,#footer_social li").hover(function() {
		$(this).find('a').stop(true).fadeTo(350,1);
	},function() {
		$(this).find('a').stop(true).fadeTo(350,0);
	});

	$(document).ready(function() {
		$(".gform_wrapper form li input:not([type=checkbox],[type=radio],[type=submit],[type=button],[type=hidden])").each(function() {
			if ($(this).val() != "")
				$(this).parent().parent().find("label").hide();
		}).live('focus',function() {
			$(this).parent().parent().find("label").addClass("focus");
		}).live('keypress',function() {
			$(this).parent().parent().find("label").fadeOut(100);
		}).live('blur',function() {
			if ($(this).val() == "")
				$(this).parent().parent().find("label").removeClass("focus").fadeIn();
			else
				$(this).parent().parent().find("label").hide();
		});

		$(".gform_wrapper form li textarea").each(function() {
			if ($(this).val() != "")
				$(this).parent().parent().find("label").hide();
		}).live('focus',function() {
			$(this).parent().parent().find("label").addClass("focus");
		}).live('keypress',function() {
			$(this).parent().parent().find("label").fadeOut(100);
		}).live('blur',function() {
			if ($(this).val() == "")
				$(this).parent().parent().find("label").removeClass("focus").fadeIn();
			else
				$(this).parent().parent().find("label").hide();
		});

		$(document).bind('gform_post_render',function(a,b) {
			$("#gform_wrapper_"+b+" input:not([type=checkbox],[type=radio],[type=submit],[type=button],[type=hidden])").each(function() {
				if ($(this).val() != "")
					$(this).parent().parent().find("label").hide();
			});
			$("#gform_wrapper_"+b+" textarea").each(function() {
				if ($(this).val() != "")
					$(this).parent().parent().find("label").hide();
			});
		});

		$(".gform_wrapper form li select").each(function() {
			if ($(this).find("option:selected").val() != "")
				$(this).parent().parent().find("label").hide();
		}).live('change',function() {
			if ($(this).find("option:selected").val() != "")
				$(this).parent().parent().find("label").hide();
			else
				$(this).parent().parent().find("label").show();
		}).live('blur',function() {
			if ($(this).find("option:selected").val() == "")
				$(this).parent().parent().find("label").show();
		});

		$("table.ui-datepicker tbody a").live('click',function() {
			var clicked = $(this).parent().attr('onclick').split("'");
			$(clicked[1]).parent().parent().find('label').hide();
		});

		$(".gform_wrapper li.radios input").each(function() {
			if ($(this).is(':checked'))
				$(this).parent().find('label').addClass('checked');
		}).click(function() {
			$(this).parent().parent().find('label.checked').removeClass('checked');
			if ($(this).is(':checked'))
				$(this).parent().find('label').addClass('checked');
		});
	});


	if ($("body").hasClass('home'))
		$("#slides").orbit({
			animation: 'fade',
			bullets: true
		});


});
