<?php

define('EMAIL','ICRC@ucf.edu');
define('FACEBOOK','https://www.facebook.com/ucfnsc');
define('TWITTER','http://www.twitter.com/ucfnsc');
define('LINKEDIN','http://www.linkedin.com/company/university-of-central-florida');
define('YOUTUBE','http://www.youtube.com');

include('functions_shortcodes.php');

add_theme_support( "title-tag" );

add_action('template_redirect','template_redirect');
function template_redirect() {

	global $wp,$wp_query,$post;
	
	$customs = get_post_types(array('public' => true));
	$type = "";

	if( isset($wp_query->query['post_type']) ) 
		$type = $wp->query_vars["post_type"];
		
	if (is_front_page() && file_exists(get_template_directory() . '/template_home.php')) {
		include(get_template_directory() . '/template_home.php');
		die();
	} else if (is_page('2013-icrc-conference')) {
		include(get_template_directory() . '/template_icrc.php');
		die();
	} else if (is_page('sessions')) {
		include(get_template_directory() . '/session/index.php');
		die();
	} else if (is_page('speakers')) {
		include(get_template_directory() . '/speaker/index.php');
		die();
	} else if (is_page('sponsors')) {
		include(get_template_directory() . '/sponsor/index.php');
		die();
	} else if (!is_singular() && taxonomy_exists('speaker-cat')) {
		include(get_template_directory() . '/speaker/taxonomy-cat.php');
		die();
	} else if (is_array($customs) && count($customs) && in_array($type,$customs)) {
		if (is_singular() && have_posts() && file_exists(get_template_directory() . '/' . $type . '/single.php')) {
			include(get_template_directory() . '/' . $type . '/single.php');
			die();
		}
		$wp_query->is_404 = true;
	}
}

add_filter('body_class','custom_type_body_classes');
function custom_type_body_classes($classes) {
	global $post;
	if (is_page())
		$classes[] = $post->post_name;
	return $classes;
}

function get_excerpt_manual($post = '') {
	if ($post == '')
		global $post;
	if (!is_object($post))
		$post = get_post($post);

	if ($post->post_excerpt != '')
		echo apply_filters('the_excerpt',$post->post_excerpt . new_excerpt_more(''));
	else
		echo wp_trim_words(apply_filters('the_excerpt',$post->post_content),25,'') . new_excerpt_more('');
}

add_filter('excerpt_more', 'new_excerpt_more');
function new_excerpt_more($more) {
	global $post;
	return '...<a class="readmore" href="'. get_permalink($post->ID) . '">Read More</a> &raquo;';
}

function antispam($text) {
	$emailNOSPAMaddy = '';
	$mailto = 0;
	$emailaddy = $text;
	srand ((float) microtime() * 1000000);
	for ($i = 0; $i < strlen($emailaddy); $i = $i + 1) {
		$j = floor(rand(0, 1+$mailto));
		if ($j==0) {
			$emailNOSPAMaddy .= '&#'.ord(substr($emailaddy,$i,1)).';';
		} elseif ($j==1) {
			$emailNOSPAMaddy .= substr($emailaddy,$i,1);
		} elseif ($j==2) {
			$emailNOSPAMaddy .= '%'.zeroise(dechex(ord(substr($emailaddy, $i, 1))), 2);
		}
	}
	$emailNOSPAMaddy = str_replace('@','&#64;',$emailNOSPAMaddy);
	return $emailNOSPAMaddy;
}

function is_ipad() {
	$is_ipad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');
	if ($is_ipad)
		return true;
	else return false;
}

add_filter('body_class','body_class_names');
function body_class_names($classes) {
    global $is_lynx, $is_gecko, $is_winIE, $is_macIE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
    if($is_lynx) $classes[] = 'lynx';
    if($is_gecko) $classes[] = 'gecko';
    if($is_winIE) $classes[] = 'winIE';
    if($is_macIE) $classes[] = 'macIE';
    if($is_opera) $classes[] = 'opera';
    if($is_NS4) $classes[] = 'NS4';
    if($is_safari) $classes[] = 'safari';
    if($is_chrome) $classes[] = 'chrome';
	if(is_ipad()) $classes[] = 'ipad';
    elseif($is_iphone) $classes[] = 'iphone';

	if (!$is_iphone && !is_ipad())
		$classes[] = 'awesomeness';

    return $classes;
}

//add_action('wp_nav_menu_objects','nav_arrows',10,2);
function nav_arrows($items,$args) {
	if ($args->theme_location != 'primary')
		return $items;

	foreach ($items as $k => $item)
		if ($item->menu_item_parent == 0)
			$items[$k]->title = $item->title . ' <span>&raquo;</span>';
		else
			$items[$k]->title = '<span>&laquo;</span> ' . $item->title;

	return $items;
}

function bco() { echo '<a id="bco" href="http://www.brandco.com" target="_blank">Made by BrandCo</a>'; }

add_action('wp_head','facebook_meta_tags');
function facebook_meta_tags() {
	if (!is_single())
		return;

	if (has_post_thumbnail()) {
		$id = get_post_thumbnail_id();
		$img = wp_get_attachment_image_src($id,'thumbnail');
		echo '<link rel="image_src" href="' . $img[0] . '" />';
	}
}

add_filter( 'user_contactmethods','add_socialmedia_contactmethod',11);
function add_socialmedia_contactmethod( $contactmethods ) {
  // Add Twitter
  if (!isset($contactmethods['twitter']))
	  $contactmethods['twitter'] = 'Twitter';

  // Add Facebook
  if (!isset($contactmethods['facebook']))
	  $contactmethods['facebook'] = 'Facebook';

  // Add LinkedIn
  if (!isset($contactmethods['linkedin']))
	  $contactmethods['linkedin'] = 'LinkedIn';

  // Add YouTube
  if (!isset($contactmethods['youtube']))
	  $contactmethods['youtube'] = 'YouTube';

  // Remove Yahoo IM
  if (isset( $contactmethods['yim']))
	  unset( $contactmethods['yim'] );

  return $contactmethods;
}




/*if (isset($_POST) && is_array($_POST) && count($_POST) && $_POST['realistic'] == '' && isset($_POST['formname'])) {
	$to = 'caleb@arrangingpixels.com';
	$subject = 'Test Contact form';
	$from = '';

	$msg .= '';
	
	$labels = array(
		'fname' => 'First Name',
		'lname' => 'Last Name',
		'email' => 'Email',
		'phone' => 'Phone',
		'comments' => 'Comments',
	);
	
	foreach ($_POST as $k => $v)
		if ($k == 'realistic' || $k == 'formname')
			continue;
		else
			if (array_key_exists($k,$labels))
				$msg .= $labels[$k] . ': ' . stripslashes(trim($v)) . "\n\n";
			else
				$msg .= ucfirst($k) . ': ' . stripslashes(trim($v)) . "\n\n";
		
	$ref = $_SERVER['HTTP_REFERER'];
	if (mail($to,$subject,$msg,"From: $from"))
		wp_redirect(add_query_arg('success',1,remove_query_arg('unable',$ref)));
	else
		wp_redirect(add_query_arg('unable',1,$ref));

	exit();
}*/

?>
