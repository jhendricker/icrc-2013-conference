<?php

if ( function_exists('register_sidebar') ) {
	
	function create_sidebars($names) {
		$args = array('before_widget' => '<div id="%1$s" class="widget builtin %2$s">','after_widget' => '</div>','before_title' => '<h4 class="widgettitle">','after_title' => '</h4>');
		foreach ($names as $name) {
			if (!is_array($name))
				register_sidebar(array_merge($args,array('name'=>$name)));
			else
				register_sidebar(array_merge($args,$name));
		}

	}

	$sidebars = array(
		array('name' => 'Blog','id' => 'blog'),
		array('name' => 'Conference','id' => 'conference'),
		array('name' => 'Contact','id' => 'contact'),
		array('name' => 'Home','id' => 'home'),
		array('name' => 'Speakers','id' => 'speakers'),
		array('name' => 'Sponsors','id' => 'sponsors')
	);

	$sort = array();
	foreach ($sidebars as $sidebar)
		if (is_array($sidebar))
			$sort[] = $sidebar['name'];
		else
			$sort[] = $sidebar;
	array_multisort($sort,$sidebars);

	$sidebars[] = array('name' => 'Default','id' => 'default');

	create_sidebars($sidebars);

}

$widgets = array();

$widgets[] = 'qconnect';
class qconnect extends WP_Widget {

	var $title = 'Quick Connect';

	function qconnect() {
		parent::__construct( false, $this->title );
	}

	function widget( $args, $instance ) {
		echo '<div id="qconnect" class="widget">';
			echo '<h4 class="widgettitle">Quick Connect</h4>';
			if (isset($_GET['success']))
				echo '<p>Thank you for contacting us!</p>';
			else {
				echo '<form method="post" action="">';
					echo '<input type="text" name="realistic" value="" />';
					echo '<input type="hidden" name="form_name" value="Quick Connect" />';
					if (isset($_GET['unable']) && $_GET['unable'] == 2)
						echo '<p>Unable to submit form. Please try again.</p>';
					echo '<ul>';
						echo '<li>';
							echo '<label for="qc_name">Full Name*</label>';
							echo '<input type="text" name="name" id="qc_name" class="required" />';
						echo '</li><li>';
							echo '<label for="qc_email">Email Address*</label>';
							echo '<input type="email" name="email" id="qc_email" class="required email" />';
						echo '</li><li>';
							echo '<label for="qc_comments">Comments</label>';
							echo '<textarea name="comments" id="qc_comments"></textarea>';
						echo '</li><li>';
							echo '<input type="submit" value="Submit &raquo;" />';
						echo '</li>';
					echo '</ul>';
					echo '<script type="text/javascript"> jQuery(function($) { $("#qconnect form").validate(); });</script>';
				echo '</form>';
			}
		echo '</div>';
	}
}

$widgets[] = 'latests';
class latests extends WP_Widget {

	var $title = 'Latest Blog Articles';

	function latests() {
		parent::__construct( false, $this->title );
	}

	function excerpt_length( $length ) {
		return 15;
	}

	function widget( $args, $instance ) {
		add_filter('excerpt_length',array(&$this,'excerpt_length'),999);

		$posts = new WP_Query('posts_per_page=5');
		echo '<div id="latests" class="widget">';
			echo '<h4 class="widgettitle">Our Blog</h4>';
			echo '<ul>';
				while ($posts->have_posts()) {
					$posts->the_post();
					echo '<li id="post-' . get_the_ID() . '">';
						echo '<h2><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>';
						echo '<div class="entry">';
							the_excerpt();
						echo '</div>';
					echo '</li>';
				}
			echo '</ul>';
		echo '</div>';

		remove_filter('excerpt_length',array(&$this,'excerpt_length'));

		wp_reset_query();
		wp_reset_postdata();
	}

}

//$widgets[] = 'location';
class location extends WP_Widget {

	var $title = 'Our Location';

	var $data = array(
		'address' => array(
			'default' => "6009 Beltline Road\r\nSuite 101\r\nDallas, Texas 75254",
			'label' => 'Address',
			'type' => 'textarea'
		),
		'main' => array('default' => '972-638-9800','label' => 'Main'),
		'fax' => array('default' => '972-644-0330','label' => 'Fax'),
		'email' => array('default' => 'info@teamrobbins.com','label' => 'Email','type' => 'email')
	);

	function location() {
		parent::__construct( false, $this->title );
	}

	function widget( $args, $instance ) {
		echo '<div id="location" class="widget">';
			echo '<h4 class="widgettitle">Our Location</h4>';
			echo nl2br(stripslashes($instance['address'])) . '<br />';
			echo $instance['main'] . ' <span>Main</span><br />';
			echo $instance['fax'] . ' <span>Fax</span><br />';
			echo antispam($instance['email']);
		echo '</div>';
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		if (is_array($this->data) && count($this->data))
			foreach ($this->data as $key => $na)
				$instance[$key] = $new_instance[$key];

		return $instance;
	}

	function form( $instance ) {
		if (is_array($this->data) && count($this->data))
			foreach ($this->data as $key => $array) {
				if ($instance)
					$val = esc_attr($instance[$key]);
				else
					$val = $array['default'];

				$label = ucfirst($key);
				if (array_key_exists('label',$array))
					$label = $array['label'];

				echo '<strong>' . $label . '</strong><br />';
				$type = 'text';
				if (isset($array['type']))
					$type = ($array['type'] == '' ? 'text' : $array['type']);

				if ($type == 'textarea')
					echo '<textarea name="' . $this->get_field_name($key) . '" style="width: 100%;">' . $val . '</textarea><br /><br />';
				else
					echo '<input type="' . $type . '" name="' . $this->get_field_name($key) . '" value="' . $val . '" style="width: 100%;" /><br /><br />';
			}
	}
}

//$widgets[] = 'testimonials';
class testimonials extends WP_Widget {

	var $title = 'Testimonials';

	function testimonials() {
		parent::__construct( false, $this->title );
	}

	function widget( $args, $instance ) {
		if (has_filter('the_content','st_add_widget'))
			remove_filter('the_content','st_add_widget');
		if (has_filter('get_the_excerpt','st_remove_st_add_link'))
			remove_filter('get_the_excerpt','st_remove_st_add_link',9);
		if (has_filter('the_excerpt','st_add_widget'))
			remove_filter('the_excerpt','st_add_widget');

		$testimonials = new WP_Query('post_type=testimonial&posts_per_page=1&orderby=rand');
		echo '<div id="testimonials" class="widget">';
			echo '<h4 class="widgettitle">Client Testimonial</h4>';
			while ($testimonials->have_posts()) {
				$testimonials->the_post();
				echo '<div class="entry">';
					the_content();
					echo '<p class="byline">&#151; ' . get_the_title() . '</p>';
				echo '</div>';
			}
		echo '</div>';

		if (function_exists('st_add_widget')) {
			add_filter('the_content','st_add_widget');
			add_filter('get_the_excerpt','st_remove_st_add_link',9);
			add_filter('the_excerpt','st_add_widget');
		}

		wp_reset_query();
		wp_reset_postdata();
	}
}

$widgets[] = 'button';
class button extends WP_Widget {

	var $title = 'Button';

	var $data = array(
		'text' => array(
			'default' => "Button Text\r\nSecond Line",
			'label' => 'Text',
			'type' => 'textarea'
		),
		'link' => array(
			'default' => "#",
			'label' => 'Link',
		)
	);

	function button() {
		parent::__construct( false, $this->title );
	}

	function widget( $args, $instance ) {
		echo '<a class="widget button" href="' . $instance['link'] . '">' . wpautop(stripslashes($instance['text'])) . '</a>';
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		if (is_array($this->data) && count($this->data))
			foreach ($this->data as $key => $na)
				$instance[$key] = $new_instance[$key];

		return $instance;
	}

	function form( $instance ) {
		if (is_array($this->data) && count($this->data))
			foreach ($this->data as $key => $array) {
				if ($instance)
					$val = esc_attr($instance[$key]);
				else
					$val = $array['default'];

				$label = ucfirst($key);
				if (array_key_exists('label',$array))
					$label = $array['label'];

				echo '<strong>' . $label . '</strong><br />';
				if ($array['type'] == 'textarea')
					echo '<textarea name="' . $this->get_field_name($key) . '" style="width: 100%;">' . $val . '</textarea><br /><br />';
				else
					echo '<input type="text" name="' . $this->get_field_name($key) . '" value="' . $val . '" style="width: 100%;" /><br /><br />';
			}
	}
}

$widgets[] = 'register';
class register extends WP_Widget {

	var $title = 'Register';

	var $data = array(
		'text' => array(
			'default' => 'Lorem ipsum dolor sit amet',
			'label' => 'Text',
		),
		'link' => array('default' => '#','label' => 'Link')
	);

	function register() {
		parent::__construct( false, $this->title );
	}

	function widget( $args, $instance ) {
		echo '<div id="register" class="widget">';
			echo '<h4 class="widgettitle">Register</h4>';
			echo '<div class="entry">';
				echo wpautop($instance['text']);
			echo '</div>';
			echo '<br style="clear: both;" />';
			echo '<br style="clear: both;" />';
			echo '<a class="btn" href="' . $instance['link'] . '" target="_blank">Click here!</a>';
		echo '</div>';
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		if (is_array($this->data) && count($this->data))
			foreach ($this->data as $key => $na)
				$instance[$key] = $new_instance[$key];

		return $instance;
	}

	function form( $instance ) {
		if (is_array($this->data) && count($this->data))
			foreach ($this->data as $key => $array) {
				if ($instance)
					$val = esc_attr($instance[$key]);
				else
					$val = $array['default'];

				$label = ucfirst($key);
				if (array_key_exists('label',$array))
					$label = $array['label'];

				echo '<strong>' . $label . '</strong><br />';
				$type = 'text';
				if (isset($array['type']))
					$type = ($array['type'] == '' ? 'text' : $array['type']);

				if ($type == 'textarea')
					echo '<textarea name="' . $this->get_field_name($key) . '" style="width: 100%;">' . $val . '</textarea><br /><br />';
				else
					echo '<input type="' . $type . '" name="' . $this->get_field_name($key) . '" value="' . $val . '" style="width: 100%;" /><br /><br />';
			}
	}
}

$widgets[] = 'sponsors';
class sponsors extends WP_Widget {

	var $title = 'Sponsors';

	/* var $data = array(
		'field_id' => array(
			'default' => 'DEFAULT VALUE\r\nSECOND LINE',
			'label' => 'FIELD LABEL',
			'type' => 'TEXTAREA || TEXT'
		)
	); */

	function sponsors() {
		parent::__construct( false, $this->title );
	}

	function widget( $args, $instance ) {
		echo '<div id="sponsors" class="widget">';
			echo '<h4 class="widgettitle">Our Sponsors</h4>';
			echo '<ul>';
			$sponsors = new WP_Query('post_type=sponsor&posts_per_page=3');
			while ($sponsors->have_posts()) {
				$sponsors->the_post();

				echo '<li id="post-' . get_the_ID() . '">';
					MultiPostThumbnails::the_post_thumbnail('sponsor','grayscale-image',$post->ID,'thumbnail');
					echo '<div class="title">';
						the_title();
					echo '</div>';
				echo '</li>';
			}
			echo '</ul>';
			echo '<a class="btn" href="http://www.centerforcrisiscommunication.com/sponsors/">View More</a>';
		echo '</div>';
	}
}

$widgets[] = 'contact_form';
class contact_form extends WP_Widget {

	var $title = 'Contact Form';

	function contact_form() {
		parent::__construct( false, $this->title );
	}

	function widget( $args, $instance ) {
		echo '<div id="contactform" class="widget">';
			echo '<h4 class="widgettitle">Contact Us</h4>';
			echo do_shortcode('[gravityform id="2" name="Sidebar: Contact Us" title="false" ajax="true"]');
		echo '</div>';
	}

}

$widgets[] = 'stayinformed';
class stayinformed extends WP_Widget {

	var $title = 'Stay Informed Form';

	function stayinformed() {
		parent::__construct( false, $this->title );
	}

	function widget( $args, $instance ) {
		echo '<div id="stayinformed" class="widget">';
			echo '<h4 class="widgettitle">Stay Informed</h4>';
			echo do_shortcode('[gravityform id="1" name="Sidebar: Stay Informed" title="false" ajax="true"]');
		echo '</div>';
	}

}




//$widgets[] = 'skeleton';
class skeleton extends WP_Widget {

	var $title = '';

	/* var $data = array(
		'field_id' => array(
			'default' => 'DEFAULT VALUE\r\nSECOND LINE',
			'label' => 'FIELD LABEL',
			'type' => 'TEXTAREA || TEXT'
		)
	); */

	function skeleton() {
		parent::__construct( false, $this->title );
	}

	function widget( $args, $instance ) {
		echo '<div id="" class="widget">';
			echo '<h4 class="widgettitle"></h4>';
		echo '</div>';
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		if (is_array($this->data) && count($this->data))
			foreach ($this->data as $key => $na)
				$instance[$key] = $new_instance[$key];

		return $instance;
	}

	function form( $instance ) {
		if (is_array($this->data) && count($this->data))
			foreach ($this->data as $key => $array) {
				if ($instance)
					$val = esc_attr($instance[$key]);
				else
					$val = $array['default'];

				$label = ucfirst($key);
				if (array_key_exists('label',$array))
					$label = $array['label'];

				echo '<strong>' . $label . '</strong><br />';
				$type = 'text';
				if (isset($array['type']))
					$type = ($array['type'] == '' ? 'text' : $array['type']);

				if ($type == 'textarea')
					echo '<textarea name="' . $this->get_field_name($key) . '" style="width: 100%;">' . $val . '</textarea><br /><br />';
				else
					echo '<input type="' . $type . '" name="' . $this->get_field_name($key) . '" value="' . $val . '" style="width: 100%;" /><br /><br />';
			}
	}
}




sort($widgets);
add_action('widgets_init','register_widgets');
function register_widgets() {
	global $widgets;
	if (!is_array($widgets) || !count($widgets))
		return;
	foreach ($widgets as $widget)
		register_widget($widget);
}

?>
