<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

$custom = new setup_custom_post_type();
$custom->index();

get_header();

global $wp_query;
$original_query = $wp_query->query;

$new_q = array();
$new_q['post_type'] = $custom->type;
$new_q['orderby'] = 'date';
$new_q['order'] = 'desc';
$new_q['paged'] = get_query_var( 'paged' );
$new_q['posts_per_page'] = -1;

$customs = new WP_Query($new_q);
?>

	<?php if ($customs->have_posts()) : ?>

		<?php while ($customs->have_posts()) : $customs->the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<small><?php the_time('F jS, Y') ?> <!-- by <?php the_author() ?> --></small>

				<div class="entry">
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div>

				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>

<?php get_footer(); ?>
