<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>

	<br style="clear: both;" />

	<div id="footer">

		<div class="container">

			<div class="nav" id="nav_footer">
				<?php wp_nav_menu(array('theme_location' => 'footer')); ?>
			</div>

			<?php /*<div class="nav" id="nav_ccc">
				CCC:
				<?php wp_nav_menu(array('theme_location' => 'footer-ccc', 'depth' => 1)); ?>
			</div>
*/ ?>

			<div class="nav" id="nav_icrc">
				<?php wp_nav_menu(array('theme_location' => 'footer-icrc','depth' => 1,'items_wrap' => 'ICRC Conference<ul id="%1$s" class="%2$s">%3$s</ul>')); ?>
			</div>

			<div class="alignright">
				<ul id="footer_social">
					<li class="fb"><a href="<?php echo FACEBOOK ?>">Like us on Facebook</a></li>
					<li class="tw"><a href="<?php echo TWITTER ?>">Follow us on Twitter</a></li>
					<li class="in"><a href="<?php echo LINKEDIN ?>">View us on LinkedIn</a></li>
					<li class="yt"><a href="<?php echo YOUTUBE ?>">Watch us on YouTube</a></li>
				</ul>
				<a id="footer_email" href="<?php echo antispam('mailto:' . EMAIL) ?>"><?php echo antispam(EMAIL) ?></a>
				<div id="copyright">Copyright &copy; <?php echo date("Y") ?> UCF. All Rights Reserved.</div>
			</div>

			<br style="clear: both;" />

			<a id="bco" href="http://www.brandco.com" target="_blank">Made by BrandCo</a>

			<br style="clear: both;" />

		</div>

	</div>

	<?php wp_footer(); ?>

</body>
</html>
